﻿using System;
namespace task_4_1
{
    public class GameEvents
    {

        public delegate void NotifyFullMessage (string message, int number);
        public delegate void StopGame();

        public event NotifyFullMessage? Notify;
        public event StopGame? Stop;


        public void GameLogic(string consoleInput, int randomNumber)
        {
            if (consoleInput is null)
            {
                throw new ArgumentNullException();
            }

            int number;

            if (int.TryParse(consoleInput, out number))
            {
                if (randomNumber == number)
                {
                    Notify?.Invoke($"You win! Number is ", number);
                    Stop?.Invoke();
                }
                else if (randomNumber > number)
                {
                    Notify?.Invoke("Your number is less ", number);
                }
                else if (randomNumber < number)
                {
                    Notify?.Invoke("Your number is higher ", number);
                }
            }
            else
            {
                Notify?.Invoke("Incorrect ", number);
            }
        }
    }
}