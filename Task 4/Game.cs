﻿namespace task_4;
using task_4_1;
public class Game
{
    private bool stopGame { get; set; } 
    public void StartGame()
    {
        GameEvents gameEvents = new GameEvents();
        gameEvents.Notify += ShowEvents;
        gameEvents.Stop += RestartGame;
        int randomNumber = new Random().Next(1, 100);

        Console.WriteLine("Guess the number game \nguess the number from 1 to 100");
        while (!stopGame)
        {
            Console.WriteLine("Enter a number");
            string consoleInput = Console.ReadLine();
            ClearCurrentConsoleLine();
            gameEvents.GameLogic(consoleInput, randomNumber);
        }
    }
    private void ClearCurrentConsoleLine()
    {
        Console.SetCursorPosition(0, Console.CursorTop -1);
        Console.Write(new String(' ', Console.WindowWidth));
        Console.SetCursorPosition(0, Console.CursorTop);
    }

    private void ShowEvents(string message, int number) => Console.WriteLine(message + number);
    private void RestartGame()
    {
        Console.WriteLine("Start over ? Yes - y, No - n");
        if (Console.ReadKey().Key == ConsoleKey.Y)
        {
            Console.WriteLine();
            Console.Clear();
            StartGame();
        }
        else
        {
            Console.Clear();
            stopGame = true;
        }
    }
}
